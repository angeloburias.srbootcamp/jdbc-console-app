package sample;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DbOps {
	
	
	 public void SqlOperationss() throws SQLException {
		 Connection conn = DriverManager.getConnection( AccessDB.connectionString(),AccessDB.user(), AccessDB.password());		 
	}
	 
	 
	 
	 public Boolean delete(int id) throws SQLException {
		 Connection conn = DriverManager.getConnection( AccessDB.connectionString(),AccessDB.user(), AccessDB.password());		 
		 Statement stmt = conn.createStatement();
	
		if (id != 0) {
			 PreparedStatement pstmt6 = conn.prepareStatement(
					    "DELETE FROM users WHERE employeeID = ?");
			 
			 pstmt6.setInt(1, id ); 
				
			 System.out.println("Deleted Successfully ID No." + id);
			 pstmt6.executeUpdate();
		}
		
		
		 stmt.close();
		 conn.close();
		 
		 return true;
	}
	 
	 public Boolean add(int id, String name, int age, Date birthdate,  Boolean isPromotional, double salary) throws SQLException {
		 Connection conn = DriverManager.getConnection( AccessDB.connectionString(),AccessDB.user(), AccessDB.password());		 
		 Statement stmt = conn.createStatement();
		 PreparedStatement pstmt1 = conn.prepareStatement(
				    "INSERT INTO users(employeeID, name, age,birthdate, isPromotion, salary ) " +
				    " VALUES (?, ?, ?, ?, ?,? )");
		 
			pstmt1.setInt( 1, id );
			pstmt1.setString( 2, name ); 
			pstmt1.setInt( 3, age ); // please use "getFir…" instead of "GetFir…", per Java conventions.
			pstmt1.setDate( 4, birthdate );
			pstmt1.setBoolean( 5, isPromotional );
			pstmt1.setDouble( 6, salary );
			
		
			System.out.println("ID: " + id);
			System.out.println("Name: " + name);
			System.out.println("Age: " + age);
			System.out.println("Birthdate: " + birthdate);
			System.out.println("Promotional: " + isPromotional);
			System.out.println("Salary" + salary);
			
		 pstmt1.executeUpdate();
		 stmt.close();
		 conn.close();
		 
		 return true;
	}
	 
	 public Boolean update(int id,String name, int age, Date birthdate,  Boolean isPromotional, double salary) throws SQLException {
		 Connection conn = DriverManager.getConnection( AccessDB.connectionString(),AccessDB.user(), AccessDB.password());		 
		 Statement stmt = conn.createStatement();
		 PreparedStatement pstmt4 = conn.prepareStatement(
				    "UPDATE  users SET name = ?,age =?, birthdate =?, isPromotion =? salary=? WHERE employeeID = ?");
			
		 pstmt4.setString( 1, name );
		 pstmt4.setInt( 2, age); 
		 pstmt4.setDate(3,birthdate); 
		 pstmt4.setBoolean( 4, isPromotional); 
		 pstmt4.setDouble( 5, salary); 
		 pstmt4.setInt( 6, id); 

			System.out.println("ID: " + id);
			System.out.println("Name: " +""+ "JOSHUA");
			System.out.println("Age: " + age);
			System.out.println("Birtdate: " + birthdate);
			System.out.println("Is Promotional? " + isPromotional);
			System.out.println("Salary" + salary);
			
			stmt.close();
			conn.close();
			
			return true;
	}
	 
	 public void getAll() throws SQLException {
		 Connection conn = DriverManager.getConnection( AccessDB.connectionString(),AccessDB.user(), AccessDB.password());		 
		 Statement stmt = conn.createStatement();
	
		 System.out.println("All records: ");
		 ResultSet rs = stmt.executeQuery("SELECT * FROM users");
		 
	}
	 

	
	
}
