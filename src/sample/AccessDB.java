package sample;

public class AccessDB {
	
	
	private static String conString = "jdbc:mysql://localhost:3306/employee";
	private static String user = "root";
	private static String pass = "";
	
	
	public static String connectionString() {
		return conString;
	}
	
	public static String user() {
		return user;
	}
	
	public static String password() {
		return pass;
	}
	
	
	
}
