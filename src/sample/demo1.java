package sample;

import java.sql.*;
import java.time.LocalDate;

public class demo1 {

	public static void main(String[] args) throws SQLException {
		 
		 Date date1 = Date.valueOf(LocalDate.of(1997, 04, 23));
		 Date date2 = Date.valueOf(LocalDate.of(1997, 04, 23));
		 Date date3 = Date.valueOf(LocalDate.of(1984, 04, 14));
		 Date date4 = Date.valueOf(LocalDate.of(1998, 8,23 ));
		 
		 DbOps ops = new DbOps();
		 
		if (ops.add(1, "ANGELO", 20, date1, true, 10000.0)
		 && ops.add(2, "JOHN DOE", 0, date2, true, 25000.0)
		 && ops.add(3, "HITLER", 3, date3, true, 99000.0)
		 && ops.update(1, "JOSHUA", 0, date4, true, 10000.0)
		 && ops.update(3, null, 0, date4, true, 99000.0)
		 && ops.delete(1)) 
		{	 
			 System.out.println("Database updated successfully ");
			 System.out.println("All queries run successfully ");
		}
		else 
		{
			 System.out.println("Database not updated successfully ");
		}
					
	}	
}
